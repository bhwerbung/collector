package de.wagentim.sites;

import de.wagentim.crawler.ICrawler;

public interface ISite
{
	public void run();
    public void setCrawler(ICrawler crawler);
    public String getStartLink();
    public String getSiteName();
}
