package de.wagentim.utils;

import java.util.UUID;

public final class IDUtil
{
	public static String getUUID()
	{
		return UUID.randomUUID().toString();
	}
}
