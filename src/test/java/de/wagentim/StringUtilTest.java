package de.wagentim;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.wagentim.utils.StringUtil;

/**
 * Unit test for simple App.
 */
public class StringUtilTest 
{
    private StringUtil su = null;

    @Before
    public void init(){
        su = new StringUtil();
    }

    @Test
    public void getSitePackageNameTest()
    {
        String name = StringUtil.getSitePackageName();
        assertTrue(name.equals("de.wagentim.sites."));
    }
    
}
